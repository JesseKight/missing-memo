using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Code used and adapted from the Gaming and Simulation book
 */
public class PlayerMovement : MonoBehaviour
{
    float speed = 0.075f;
    float dodgeSpeed = 5.0f;
    float dodgeCooldown = 0.0f;
    float dodgeTime = 1.25f;
    
    // Start is called before the first frame update
    void Start()
    {
       
    }

    
    // Update is called once per frame
    void Update()
    {
        
        
        if (Input.GetKey(KeyCode.W))
        {
            
            this.transform.Translate(0, speed, 0);

            

        }
        if (Input.GetKey(KeyCode.S))
        {
          
            this.transform.Translate(0, -speed, 0);

            

        }
        if (Input.GetKey(KeyCode.A))
        {

            this.transform.Translate(-speed, 0, 0);

            


        }
        if (Input.GetKey(KeyCode.D))
        {
           
            this.transform.Translate(speed, 0, 0);

            


        }
        if(dodgeCooldown <= 0)
        {
            if (Input.GetKey(KeyCode.W) && Input.GetKeyDown(KeyCode.Space))
            {
                this.transform.Translate(0, dodgeSpeed, 0);
                dodgeCooldown = dodgeTime;
            }

            if (Input.GetKey(KeyCode.S) && Input.GetKeyDown(KeyCode.Space))
            {
                this.transform.Translate(0, -dodgeSpeed, 0);
                dodgeCooldown = dodgeTime;
            }

            if (Input.GetKey(KeyCode.A) && Input.GetKeyDown(KeyCode.Space))
            {
                this.transform.Translate(-dodgeSpeed, 0, 0);
                dodgeCooldown = dodgeTime;
            }

            if (Input.GetKey(KeyCode.D) && Input.GetKeyDown(KeyCode.Space))
            {
                this.transform.Translate(dodgeSpeed, 0, 0);
                dodgeCooldown = dodgeTime;
            }
            
        }
        dodgeCooldown -= Time.deltaTime;

    }
}
