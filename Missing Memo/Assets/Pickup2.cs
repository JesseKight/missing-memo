using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup2 : MonoBehaviour
{

    public Transform player, WeaponHolder;
    
    public Rigidbody2D rb;
    public BoxCollider2D coll;

    public float pickUpRange;
    public float dropForce;

    public Fire gunScript;

    public bool equipped = true;
    public static bool slotFull;



    // Start is called before the first frame update
    void Start()
    {
        if (!equipped)
        {

            rb.isKinematic = false;
            coll.isTrigger = false;
        }

        if (equipped)
        {

            rb.isKinematic = true;
            coll.isTrigger = true;

        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 distanceToPlayer = player.position - transform.position;
        if (!equipped && distanceToPlayer.magnitude <= pickUpRange && Input.GetKeyDown(KeyCode.E) && !slotFull)
        {
            PickUp();
            
        }
        
        if (equipped && Input.GetKeyDown(KeyCode.Q))
            Drop();
    }

    private void PickUp()
    {
        equipped = true;
        slotFull = true;
        

        transform.SetParent(WeaponHolder);
        transform.localPosition = new Vector3(0, 0, 0);


        rb.isKinematic = true;
        coll.isTrigger = true;
        gunScript.enabled = true;


    }

    private void Drop()
    {
        equipped = false;
        slotFull = false;

        transform.SetParent(null);

        rb.isKinematic = false;
        coll.isTrigger = false;

        rb.velocity = player.GetComponent<Rigidbody2D>().velocity;

        rb.AddForce(player.forward * dropForce, ForceMode2D.Impulse);

        gunScript.enabled = false;

    }
}
