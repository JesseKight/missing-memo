﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Credit: https://www.youtube.com/watch?v=FgI8cgYAewM

public class Shooter : MonoBehaviour
{
    public GameObject bullet;
    public Transform start;
    public float bulletCooldown = 0f;
    public float bulletTime = .10f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 gunPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if(gunPosition.x < transform.position.x)
        {
            transform.eulerAngles = new Vector3(transform.rotation.x, 180f, transform.rotation.z);
        }
        else
        {
            transform.eulerAngles = new Vector3(transform.rotation.x, 0f, transform.rotation.z);
        }
        if(Input.GetMouseButton(0))
        {
            Shooting();
        }
    }

    void Shooting()
    {
        if (bulletCooldown <= 0)
        {
            GameObject shoot = Instantiate(bullet, start.transform.position, start.transform.rotation);
            bulletCooldown = bulletTime;
            Destroy(shoot, .5f);
        }
        bulletCooldown -= Time.deltaTime;
    }

}
